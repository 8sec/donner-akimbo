﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEditor;
using AppsFlyerSDK;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    [ReadOnly] public int CurrentLevelIndex;
    public List<Level> levelHolder;
    public bool forceLevel = false;

    [ShowIf("forceLevel")]
    public int forceLevelIndex = 0;

    [Space]
    private const string Key_CurrentLevel = "LM_CurrentLevel";

    // Use this for initialization
    public virtual void Awake()
    {
        Instance = this;
        CurrentLevelIndex = PlayerPrefs.GetInt(Key_CurrentLevel, 1);
    }

    public int GetLevelIndex(){
        int indexLevel;
        if(forceLevel){
            indexLevel = forceLevelIndex - 1;
        }else{
            indexLevel = (CurrentLevelIndex - 1) % levelHolder.Count;
        }

        return indexLevel;
    }

    public Level GetCurrentLevel(){
        int indexLevel = GetLevelIndex();
        return levelHolder[indexLevel];
    }

   public void LoadLevel(){
        int indexLevel = GetLevelIndex();

        for (int i = 0; i < levelHolder.Count; i++)
        {
            if(i == indexLevel){
                levelHolder[i].gameObject.SetActive(true);
                
            }else{
                levelHolder[i].gameObject.SetActive(false);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LevelUp();
        }
    }

    public void LevelUp()
    {
        CurrentLevelIndex++;
        PlayerPrefs.SetInt(Key_CurrentLevel, CurrentLevelIndex);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_levelIndex", (CurrentLevelIndex - 1).ToString());
        AppsFlyer.sendEvent("af_Progress_LevelSuccess", richEvent);
    }

    public void LoadNextLevel(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        print("Load Next Level");
    }

    public void SkipLevel(){
        LevelUp();
        LoadNextLevel();
    }

    public void LoadLevel(string levelName){
        SceneManager.LoadScene(levelName, LoadSceneMode.Single);
    }

    
    [Button("ClearPlayerPref")]
    public void ClearPlayerPref(){
        PlayerPrefs.DeleteAll();
        if(GetComponent<ReadCurentLevelIndex>()){
            GetComponent<ReadCurentLevelIndex>().Start();
        }
    }

}