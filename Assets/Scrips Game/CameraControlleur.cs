﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using Sirenix.OdinInspector;

public class CameraControlleur : MonoBehaviour
{
    [Title("Settings")]
    public bool followRotation = true;
    [Range(0f, 1f)] public float strenghRotation = 1f;


    [Title("Reference")]
    public FollowGameObject followGameObject;
    public PathCreator spline;
    public PlayerControlleur playerControlleur;

    // Start is called before the first frame update
    void Start()
    {
        playerControlleur = GameControlleur.Instance.GetPlayerControlleur();
        
        followGameObject = GetComponent<FollowGameObject>();
        followGameObject.Target = null;
        transform.position = playerControlleur.transform.position;
        followGameObject.Target = playerControlleur.transform;
    }

    public void SetSplineFollower(PathCreator newPath){
        spline = newPath;

    }

    void LateUpdate()
    {
        
        if(followRotation){
            // Rotation
            float distance = playerControlleur.DistanceTravelled;
            Vector3 currentSlineDirection = spline.path.GetDirectionAtDistance(distance, EndOfPathInstruction.Stop);
            transform.forward = Vector3.Slerp(Vector3.forward, currentSlineDirection, strenghRotation);            
        }
    }
}
