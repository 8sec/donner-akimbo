﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBox : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<Ennemy>()){
            other.GetComponent<Ennemy>().Die();
        }
    }
}
