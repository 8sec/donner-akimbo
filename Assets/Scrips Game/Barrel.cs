﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class Barrel : MonoBehaviour
{
    [Title("Settings")]
    public bool showGizmos = false;
    public float radius = 3f;
    public int lifepoint = 10;
    private int originalLifePoint;
    public int dammage = 10;
    public LayerMask layerMaskCollision;

    [Title("Reference")]
    public MMFeedbacks explode_feedback;
    public MMFeedbacks hit_feedback;
    public ParticleSystem bombeFuse;

    private void Start() {
        originalLifePoint = lifepoint;
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.GetComponent<Bullet>()){
            other.transform.GetComponent<Bullet>().KillBullet();
            Hit();

            if(lifepoint <= 0){
                Explode();
            }
        }
    }

    public void Hit(){
        lifepoint -= 1;
        hit_feedback.PlayFeedbacks();

        if(lifepoint <= (int) (originalLifePoint / 2)){
            bombeFuse.Play();
        }
    }

    private void Explode(){
        bombeFuse.Stop();
        explode_feedback.PlayFeedbacks();

        Collider[] colliderHit = Physics.OverlapSphere(transform.position, radius, layerMaskCollision);

        for (int i = 0; i < colliderHit.Length; i++)
        {
            if(colliderHit[i].GetComponent<Ennemy>()){
                colliderHit[i].GetComponent<Ennemy>().Hit(dammage);
            }

            if(colliderHit[i].GetComponent<CrateBox>()){
                colliderHit[i].GetComponent<CrateBox>().PushBoxExplosion(transform);
            }
        }
    }

    private void OnDrawGizmosSelected() {
        if(showGizmos){
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position , radius);
        }
    }
}
