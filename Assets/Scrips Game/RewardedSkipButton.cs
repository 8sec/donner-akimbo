﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedSkipButton : RewardedButton
{
    public override void CollectReward(){
        LevelManager.Instance.SkipLevel();
    }
}
