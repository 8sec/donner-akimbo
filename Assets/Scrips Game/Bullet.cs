﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using MoreMountains.Feedbacks;

public class Bullet : MonoBehaviour
{
    [Title("Settings")]
    public float speedBullet = 1f;
    public float delayBeforeSkrink = 0.3f;
    public float durationSkrinh = 2f;

    [Title("Reference")]
    public MMFeedbacks feedback_die;
    private new Rigidbody rigidbody;
    private void Start() {
        rigidbody = GetComponent<Rigidbody>();
        Destroy(gameObject, (durationSkrinh + delayBeforeSkrink));
        transform.DOScale(0f, durationSkrinh).SetDelay(delayBeforeSkrink);
        
    }
    void FixedUpdate()
    {
        rigidbody.MovePosition(transform.position + (transform.forward * Time.fixedDeltaTime * speedBullet));
    }

    public void KillBullet(){
        feedback_die.PlayFeedbacks();
        Destroy(gameObject);
    }

    
}
