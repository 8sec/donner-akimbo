﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class Arrival : MonoBehaviour
{
    public MMFeedbacks finishline_feedback;
    private void OnTriggerEnter(Collider other) {
        if(other.GetComponent<PlayerControlleur>()){
            finishline_feedback.PlayFeedbacks();
        } 
    }
}
