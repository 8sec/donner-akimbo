﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using Sirenix.OdinInspector;
using DG.Tweening;
using MoreMountains.Feedbacks;

public class PlayerControlleur : MonoBehaviour
{
    [Title("Setup")]
    public bool akimboMode = true;
    public Transform armRight;
    private Vector3 lastArmRighRotation;
    public Transform armLeft;
    private Vector3 lastArmLeftRotation;

    [Title("Settings")]
    [SerializeField] private bool godMod = false;
    [HideIf("godMod")] public int lifepoint = 3;
    public bool canShoot = true;
    [SerializeField] private float durationInvinsibility = 1f;
    [ReadOnly] [SerializeField] private bool invisibility = false;

    [Title("Movement")]
    public PathCreator spline;
    public Transform spinePlayer;

    public float speed = 5;
    public float speedFinish = 8f;
    private float speed_tmp;
    public EndOfPathInstruction endOfPathInstruction;
    public bool onMovement = false;
    public Camera mainCamera;
    float distanceTravelled;
    public float DistanceTravelled
    {
        get
        {
            return distanceTravelled;
        }
    }
    [ReadOnly] public float angleShooting;
    private Vector3 lastSplineRotation;

    [Title("Shoot")]
    public bool shootWithLeftGun = false;
    public bool shootWithRightGun = false;
    public Transform shootPointRight;
    public Transform shootPointLeft;
    public GameObject bullet;
    public float cooldown = 0.1f;
    public ParticleSystem muzzleFlashRight;
    public ParticleSystem muzzleFlashLeft;
    float tmp_cooldown = 0f;
    bool shootRight = false;

    [Title("Reference")]
    public MMFeedbacks feedbacks_hit;

    // Variable
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();   
    }

    public void SetPath(PathCreator newPath){
        spline = newPath;
        SetPlayerInPosition();
    }

    private void SetPlayerInPosition(){
        if (spline != null)
        {
            // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
            spline.pathUpdated += OnPathChanged;

            // Put the player on the first point
            transform.position = spline.path.GetPoint(0);
        }
        RotateLegPlayer();
        SetStartChestRotation();
    }
    
    public void StartRunning()
    {
        speed_tmp = speed;
        onMovement = true;
        animator.enabled = true;
        animator.SetTrigger("Run");
        
    }

    public void StartAkimboMode(){
        akimboMode = true;
        StartRunning();
    }

    public void StartStandartMode(){
        akimboMode = false;
        StartRunning();
    }

    public void Lose()
    {
        onMovement = false;
        animator.SetTrigger("Lose");
        GameControlleur.Instance.LoseGame();
    }

    public void Win()
    {
        onMovement = false;
        animator.SetTrigger("Win");
        GameControlleur.Instance.WinGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (onMovement)
        {
            MovePlayerAlongSpline();
        }
    }

    private void FixedUpdate()
    {
        if (onMovement && canShoot)
        {
            Shoot();
        }
    }

    private void LateUpdate()
    {
        if (onMovement)
        {
            RotateChestPlayer();
            RotateLegPlayer();
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.GetComponent<Ennemy>()){
            Ennemy ennemy = other.transform.GetComponent<Ennemy>();
            int dammage = ennemy.dammage;
            Hit(dammage);
            ennemy.Die();
        }
    }

    private void Hit(int dammage){
        if(godMod == false && invisibility == false){
            feedbacks_hit.PlayFeedbacks();
            invisibility = true;
            animator.SetTrigger("Hit");
            lifepoint -= dammage;
            StartCoroutine(ResetInvisibility());
            if(lifepoint <= 0){
                Lose();
            }
        }
    }

    public IEnumerator ResetInvisibility(){
        yield return new WaitForSeconds(durationInvinsibility);
        invisibility = false;
    }

    private void Shoot()
    {
        tmp_cooldown += Time.deltaTime;

        if (tmp_cooldown >= cooldown)
        {
            Vector3 instanciatePosition;

                // Bullet Right
                if(shootWithRightGun){
                    instanciatePosition = shootPointRight.position;
                    Vector3 newRotation = Vector3.zero;
                    if(akimboMode == false){
                        newRotation = spinePlayer.rotation.eulerAngles.SetX(0);
                    }else{
                        newRotation = shootPointRight.rotation.eulerAngles.SetX(0);
                    }

                    Instantiate(bullet, instanciatePosition, Quaternion.Euler(newRotation));
                    muzzleFlashRight.Play();
                }

                // Bullet Left
                if(shootWithLeftGun){
                    instanciatePosition = shootPointLeft.position;

                    Vector3 newRotation = Vector3.zero;
                    if(akimboMode == false){
                        newRotation = spinePlayer.rotation.eulerAngles.SetX(0);
                    }else{
                        newRotation = shootPointLeft.rotation.eulerAngles.SetX(0);
                    }

                    Instantiate(bullet, instanciatePosition, Quaternion.Euler(newRotation));
                    muzzleFlashLeft.Play();
                }
                
            tmp_cooldown = 0f;
        }
    }

    private void RotateArmPlayer()
    {
        // Chest Follow Leg movement;
        lastSplineRotation = spinePlayer.rotation.eulerAngles;
        spinePlayer.rotation = Quaternion.Euler(lastSplineRotation.SetX(0).SetZ(0));

        float rotationAngle = GameControlleur.Instance.GetDirection().x;
        Vector3 rotationArm = new Vector3(0f, rotationAngle, 0f);
        
        // Calculate angle
        float angle = Vector3.Angle(transform.forward, shootPointRight.forward);
        // Debug.Log("&&& angle: " + angle);

        if(IsBetween(angle - rotationAngle, 7f, 90f)){
            // Left Arm
            lastArmLeftRotation = armLeft.rotation.eulerAngles;
            armLeft.rotation = Quaternion.Euler(lastArmLeftRotation);
            armLeft.Rotate(rotationArm, Space.World);

            // Right Arm
            lastArmRighRotation = armRight.rotation.eulerAngles;
            armRight.rotation = Quaternion.Euler(lastArmRighRotation);
            armRight.Rotate(-rotationArm, Space.World);
        }else{

            print("Outside of arm range");
        }
    }

    private void SetStartChestRotation(){
        spinePlayer.forward = transform.forward;
        lastSplineRotation = spinePlayer.rotation.eulerAngles;
    }


    private void RotateChestPlayer()
    {
        // Rotate Top part of the player
        float rotationAngle = GameControlleur.Instance.GetDirection().x;
        spinePlayer.rotation = Quaternion.Euler(lastSplineRotation.SetX(0).SetZ(0));

        Vector3 directionLeg = transform.forward;
        float angle = Vector3.SignedAngle(directionLeg, spinePlayer.forward, Vector3.up);
        angleShooting = angle;

        if (IsBetween(angle, -90f, 90f))
        {
            spinePlayer.Rotate(0f, rotationAngle, 0f, Space.World);
        }else{
            if(angle < -90f){
                spinePlayer.Rotate(0f, 1f, 0f, Space.World);
            }

            if(angle > 90f){
                spinePlayer.Rotate(0f, -1f, 0f, Space.World);
            }
        }
        lastSplineRotation = spinePlayer.rotation.eulerAngles;

    }

    private void RotateLegPlayer()
    {
        if (spline != null)
        {
            transform.forward = spline.path.GetDirectionAtDistance(distanceTravelled, endOfPathInstruction);
        }
    }

    private void MovePlayerAlongSpline()
    {
        if (spline != null)
        {
            distanceTravelled += speed_tmp * Time.deltaTime;
            transform.position = spline.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);

            // Check if the player have finish the run
            if (transform.position == spline.path.GetPoint(spline.path.NumPoints - 1))
            {
                Win();
            }
        }
    }

    public IEnumerator IncreaseSpeed(){
        float timePassed = 0f;
        float durationLerp = 1f;
        while(timePassed < durationLerp){
            timePassed += Time.deltaTime;
            speed_tmp = Mathf.Lerp(speed, speedFinish, timePassed / durationLerp);
            yield return null;
        }
    }


    // If the path changes during the game, update the distance travelled so that the follower's position on the new path
    // is as close as possible to its position on the old path
    void OnPathChanged()
    {
        distanceTravelled = spline.path.GetClosestDistanceAlongPath(transform.position);
    }

    public bool IsBetween(float testValue, float bound1, float bound2)
    {
        return (testValue >= Mathf.Min(bound1, bound2) && testValue <= Mathf.Max(bound1, bound2));
    }

    private float ClampAngle(float angle, float from, float to)
    {
        // accepts e.g. -80, 80
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + from);
        return Mathf.Min(angle, to);
    }

}
