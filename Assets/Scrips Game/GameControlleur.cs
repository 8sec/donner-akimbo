﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;

public class GameControlleur : MonoBehaviourSingleton<GameControlleur>
{
    [Title("Settings")]
    // Touch Control
    public float deathZone = 0f;
    public float strenghRotation = 1f;
    private float lastFrameX = 0f;
    private float lastFrameY = 0f;
    private float deltaX = 0f;
    private float deltaY = 0f;
    Vector2 deltaMove = Vector2.zero;
    Vector2 curpos = Vector2.zero;
    

    private List<Ennemy> currentEnnemysList = new List<Ennemy>();

    // Event
    public delegate void PlayerStartRun();
    public static event PlayerStartRun playerStartRun;
    public Color colorBlue;
    public Color colorPink;
    private bool gameStart = false;

    [Title("Reference")]
    public PlayerControlleur currentPlayer;
    public CameraControlleur cameraControlleur;
    public Text goldText;
    public Text levelText;
    public Text killCountText;
    public Text timerText;
    private int killCount = 0;
    private float goldValue = 0;

    private void Awake() {

    }

    private void Start() {
        currentEnnemysList.Clear();
        UIController.Instance.ShowPanel(UIPanelName.TITLE_SCREEN);
        LevelManager.Instance.LoadLevel();
        PathCreation.PathCreator newPath = LevelManager.Instance.GetCurrentLevel().GetPathLevel();
        LevelManager.Instance.GetCurrentLevel().SetThemeColor();
        currentPlayer.SetPath(newPath);
        cameraControlleur.SetSplineFollower(newPath);
        killCount = 0;
        MonetizationManager.Instance.ShowInterstitial(); // Show Inter ads when start a new game

    }


    public void AddGold(int valueToAdd, float delay){
        float newScore = goldValue + valueToAdd;
        DOTween.To(() => goldValue, x => goldValue = x, newScore, 1f).SetDelay(delay).OnStart(() =>{
            MMVibrationManager.ContinuousHaptic(0.7f, 0f, 1f);
        }).OnComplete(() =>{
            MMVibrationManager.StopContinuousHaptic();
        });
    }

    private void Update() {
        // Start the game
        if(gameStart == false && Input.GetMouseButtonDown(0)){
            StartGame();
        }

        UpdateGameplayUI();
    }

    private void UpdateGameplayUI(){
        string goldFormat = goldValue.ToString("F0");
        goldText.text = goldFormat;
        
        if(timerText){
            string timeFormat = Time.time.ToString("F0");
            timerText.text = timeFormat;
        }

    }

    public void StartGame(){
        print("Start Game");
        currentPlayer.StartStandartMode();
        gameStart = true;
        
        
        UIController.Instance.HidePanel(UIPanelName.TITLE_SCREEN);
        GameManager.Instance.StartSession();

        if(playerStartRun != null){
            playerStartRun();
        }
    }

    public void LoseGame(){
        foreach (Ennemy ennemy in currentEnnemysList)
        {
            ennemy.Taunt();
        }
        levelText.text = "LEVEL " + LevelManager.Instance.GetLevelIndex().ToString();
        killCountText.text = killCount.ToString();
        GameManager.Instance.GameOver();
    }

    public void WinGame(){
        GameManager.Instance.LevelSuccess();
    }

    public void AddEnnemyToList(Ennemy ennemy){
        currentEnnemysList.Add(ennemy);
    }

    public void RemoveEnnemyToList(Ennemy ennemy){
        currentEnnemysList.Remove(ennemy);
        killCount++;

        if(currentEnnemysList.Count <= 0){
            StartCoroutine(currentPlayer.IncreaseSpeed());
        }
    }


    public PlayerControlleur GetPlayerControlleur(){
        return currentPlayer;
    }


    

    private void ResetValue()
    {
        lastFrameX = 0f;
        lastFrameY = 0f;
        deltaX = 0f;
        deltaY = 0f;
        deltaMove = Vector2.zero;
    }

    public Vector2 GetDirection()
    {
        if (Input.GetMouseButton(0))
        {
            if (lastFrameX == 0f)
            {
                lastFrameX = Input.mousePosition.x;
                lastFrameY = Input.mousePosition.y;
                curpos = Input.mousePosition;
                deltaX = 0f;
                deltaY = 0f;
                deltaMove = Vector2.zero;
            }

            curpos = Input.mousePosition;

            float ratioX = (1242.0f / (float) Screen.width);
            deltaX = (curpos.x - lastFrameX) * ratioX;
            lastFrameX = curpos.x;

            float ratioY = (2688.0f / (float) Screen.height);
            deltaY = (curpos.y - lastFrameY) * ratioY;
            lastFrameY = curpos.y;

            deltaMove = new Vector2(deltaX, deltaY) * strenghRotation;

            if (deltaMove.magnitude > deathZone)
            {
                return deltaMove;
            }
        }else{
            ResetValue();
        }
        return Vector2.zero;

    }



}
