﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using MoreMountains.Feedbacks;

public class ChestBox : MonoBehaviour
{

    [Title("Settings")]
    public int lifepoint = 10;

    [Title("Reference")]
    public ParticleSystem death_Fx;
    public MMFeedbacks death_feedback;
    private Tween scaleBonce;
    private Vector3 originalScale;

    private void Start() {
        originalScale = transform.localScale;
        death_feedback.Initialization();
    }


    private void OnCollisionEnter(Collision other) {
        if(other.transform.GetComponent<Bullet>()){
            other.transform.GetComponent<Bullet>().KillBullet();
            Hit();

            if(lifepoint <= 0){
                Die();
            }
        }
    }

    private void Hit(){
        lifepoint -= 1;
        if(scaleBonce != null && scaleBonce.IsPlaying()){
            scaleBonce.Kill(false);
        }
        transform.localScale = originalScale;
        Vector3 punchScale = new Vector3(0f, 0.5f, 0f);
        scaleBonce = transform.DOPunchScale(punchScale, 0.15f).SetRelative(true).SetEase(Ease.OutBounce);
    }

    private void Die(){
        death_feedback.PlayFeedbacks();
        Vector3 spawnPosition = transform.position.SetY(transform.position.y + 0.5f);
        ParticleSystem goldParticle = Instantiate(death_Fx, spawnPosition, death_Fx.transform.rotation);
        Destroy(gameObject);
    }
}
