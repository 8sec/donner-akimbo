﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public enum ThemeColor{
    Blue,
    Pink,
}
public class Level : MonoBehaviour
{
    [SerializeField] private PathCreator pathLevel;
    public ThemeColor themeColor;

    public PathCreator GetPathLevel(){
        return pathLevel;
    }

    public void SetThemeColor(){
        Color colorTheme = Color.blue;

        switch(themeColor){
            case ThemeColor.Blue:
                colorTheme = GameControlleur.Instance.colorBlue;
            break;

            case ThemeColor.Pink:
                colorTheme = GameControlleur.Instance.colorPink;
            break;
        }

        Camera.main.backgroundColor = colorTheme;
        RenderSettings.fogColor = colorTheme;
    }
}
