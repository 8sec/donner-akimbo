﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateBox : MonoBehaviour
{
    public bool killBullet = false;
    public float projectionForceBullet = 10f;
    public float projectionForceExplosion = 10f;


    private void Update() {
        if(transform.position.y < - 100f){
            Destroy(gameObject);
        }
    }

    
    private void OnCollisionEnter(Collision other) {
        if(killBullet){
            if(other.transform.GetComponent<Bullet>()){
                other.transform.GetComponent<Bullet>().KillBullet();
                PushBox(other);
            }
        }
    }

    public void PushBox(Collision other){
        Vector3 direction = other.GetContact(0).point + other.transform.forward;
        direction.Normalize();
        GetComponent<Rigidbody>().AddForce(direction * projectionForceBullet, ForceMode.VelocityChange);
    }

    public void PushBoxExplosion(Transform barrel){
        Vector3 direction = transform.position - barrel.position;
        direction.Normalize();
        GetComponent<Rigidbody>().AddForce(direction * projectionForceExplosion, ForceMode.VelocityChange);
    }
}
