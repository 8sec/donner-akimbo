﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MagneticParticle : MonoBehaviour
{
    [Title("Setting")]
    public bool shrinkSize = true;
    [ShowIf("shrinkSize")] public float divideSizeBy = 2;

    [Space]
    public float distanceForCollect = 0.1f;
    public float minDelayBeforeMagnet = 1f;
    public float maxDelayBeforeMagnet = 1.5f;

    public float durationLerp = 1f;

    [Title("Gold")]
    public int goldMin = 50;
    public int goldMax = 100;
    private int goldToAdd;

    public bool debug = false;
    [ShowIf("debug")] [ReadOnly] [SerializeField] private float lerpValueMagnet = 0;
    [ShowIf("debug")] [ReadOnly] [SerializeField] private float cooldownValue = 0;

    private float startSize;
    private Vector3[] particleStartPosition;
    private float[] deltaTimeRandom;
    private float[] lerpValuePerParticle;
    private float sizeAfterTouchTheGround;
    private bool GetStartValue = false;
    private bool magnetParticle = false;
    private new ParticleSystem particleSystem;
    private ParticleSystem.Particle[] particles;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
        goldToAdd = Random.Range(goldMin, goldMax);
        particleSystem.emission.SetBurst(0, new ParticleSystem.Burst(0.0f, goldToAdd));
        GameControlleur.Instance.AddGold(goldToAdd, minDelayBeforeMagnet);
        startSize = particleSystem.startSize;
        sizeAfterTouchTheGround =  startSize / divideSizeBy;
    }

    private void InitialiseArrayParticle(){

        InitializeIfNeeded();
        int particleAlive = particleSystem.GetParticles(particles);
        particleStartPosition = new Vector3[particleAlive];
        deltaTimeRandom = new float[particleAlive];
        lerpValuePerParticle = new float[particleAlive];

        for (int i = 0; i < particleAlive; i++)
        {
            particleStartPosition[i] = particles[i].position;
            deltaTimeRandom[i] = Random.Range(minDelayBeforeMagnet, maxDelayBeforeMagnet);
            lerpValuePerParticle[i] = 0f;
        }
        GetStartValue = true;

    }


    private void LateUpdate() {
        if(particleSystem.isPlaying){
            cooldownValue += Time.deltaTime;
            if(magnetParticle == false){
                // Scrink particle size before Magnet
                InitializeIfNeeded();
                int particlesAlive = particleSystem.GetParticles(particles);
                for (int i = 0; i < particlesAlive; i++)
                {
                    particles[i].startSize = Mathf.Lerp(startSize, sizeAfterTouchTheGround, cooldownValue);
                }
                particleSystem.SetParticles(particles, particlesAlive);
            }


            if(cooldownValue >= minDelayBeforeMagnet){
                    
                // Magnet the coin to the player
                magnetParticle = true;
                if(GetStartValue == false)
                    InitialiseArrayParticle();

                InitializeIfNeeded();
                int particlesAlive = particleSystem.GetParticles(particles);
                lerpValueMagnet += Time.deltaTime / durationLerp;
                Vector3 playerPosition = GameControlleur.Instance.GetPlayerControlleur().transform.position;

                for (int i = 0; i < particlesAlive; i++)
                {
                    if(cooldownValue > deltaTimeRandom[i]){
                        lerpValuePerParticle[i] += Time.deltaTime / durationLerp;
                        particles[i].position = Vector3.Lerp(particleStartPosition[i], playerPosition, lerpValuePerParticle[i]);
                        
                        if(shrinkSize)
                            particles[i].startSize = Mathf.Lerp(sizeAfterTouchTheGround, sizeAfterTouchTheGround / divideSizeBy, lerpValueMagnet);
                    }

                    // Collect Particle
                    if(Vector3.Distance(particles[i].position, playerPosition) < distanceForCollect){
                        particles[i].startSize = 0f;
                    }
                }

                particleSystem.SetParticles(particles, particlesAlive);
            }
        }else{
            cooldownValue = 0;
            particleSystem.Stop();
        }
    }


    void InitializeIfNeeded()
    {
        if (particleSystem == null)
            particleSystem = GetComponent<ParticleSystem>();

        if (particles == null || particles.Length < particleSystem.main.maxParticles)
            particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
    }

}
