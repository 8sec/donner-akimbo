﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.AI;
using DG.Tweening;
using MoreMountains.Feedbacks;

public class Ennemy : MonoBehaviour
{
    [Title("Settings")]
    public int lifepoint = 3;
    public float speedNavMesh = 5f;
    public float speedRigibody = 5f;

    [SerializeField] private float maxDistanceBeforeNavMeshMovement = 10f;
    [SerializeField] private float maxDistanceBeforeRigibodyMovement = 5f;
    public float timeBeforeRecalculatePath = 0.25f;
    private float cooldownNavMeshCalculation = 0f;
    public int dammage = 1;
    public float speedAnimator = 1f;
    public bool CanMove = false;
    private bool onMovementRigibody = false;
    private bool onMovementNavMesh = false;
    private bool dead = false;
    public bool debug = false;
    [ReadOnly] public NavMeshPathStatus navMeshPathStatus;
    [ReadOnly] [SerializeField] private float distanceFromPlayer;

    [Title("Reference")]
    public MMFeedbacks hit_feedback;
    public MMFeedbacks dead_feedback;
    private PlayerControlleur playerControlleur;
    private NavMeshAgent navMeshAgent;
    private Vector3 originalScale;
    private Vector3 originalPosition;
    private Tween scaleBonce;
    private Animator animator;
    private new Renderer renderer;
    private NavMeshPath navMeshPath;
    private new Rigidbody rigidbody;

    [Title("Debug")]
    [SerializeField] private bool activateDebug = true;

    [ShowIf("activateDebug")]
    [SerializeField] private Color colorGizmos = Color.green;

    private void Start() {
        navMeshAgent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        renderer = GetComponentInChildren<Renderer>();
        originalScale = transform.localScale;
        playerControlleur = GameControlleur.Instance.GetPlayerControlleur();
        navMeshAgent.speed = speedNavMesh;
        originalPosition = transform.position;
        Vector3 playerPosition = GameControlleur.Instance.GetPlayerControlleur().transform.position;
        
        GameControlleur.playerStartRun += StartMove;
        animator.speed = speedAnimator;

        hit_feedback.Initialization();
        dead_feedback.Initialization();
    }




    void Update()
    {
        cooldownNavMeshCalculation += Time.deltaTime;
        if(CanMove && dead == false){
            distanceFromPlayer = Vector3.Distance(playerControlleur.transform.position, transform.position);

            if(distanceFromPlayer < maxDistanceBeforeNavMeshMovement && distanceFromPlayer > maxDistanceBeforeRigibodyMovement){
                MoveNavMesh();
            }else{
                navMeshAgent.isStopped = true;
                onMovementNavMesh = false;
            }
        }

        if(onMovementNavMesh == false && onMovementRigibody == false){
            animator.SetBool("idle", true);
        }else{
            animator.SetBool("idle", false);
        }
    }

    private void FixedUpdate() {
        if(CanMove && dead == false){
            if(distanceFromPlayer <= maxDistanceBeforeRigibodyMovement && onMovementNavMesh == false){
                MoveRigibody();
            }else{
                onMovementRigibody = false;
            }
        }
    }

    public void Taunt(){
        navMeshAgent.isStopped = true;
        CanMove = false;
        animator.SetTrigger("Taunt");
    }

    private void StartMove(){
        if(gameObject.activeInHierarchy){
            CanMove = true;
            GameControlleur.Instance.AddEnnemyToList(this);
            GameControlleur.playerStartRun -= StartMove;
        }
    }

    private void MoveRigibody(){
        animator.SetTrigger("Jump");
        onMovementRigibody = true;
        navMeshAgent.isStopped = true;
        Vector3 newPosition = transform.position + (playerControlleur.transform.position - transform.position).normalized * speedRigibody * Time.deltaTime;
        rigidbody.MovePosition(newPosition);
        transform.LookAt(playerControlleur.transform);
    }

    private void MoveNavMesh(){
        animator.SetTrigger("Run");
        navMeshAgent.isStopped = false;
        onMovementNavMesh = true;

        if(cooldownNavMeshCalculation >= timeBeforeRecalculatePath){
            navMeshAgent.SetDestination(playerControlleur.transform.position);
            cooldownNavMeshCalculation = 0f;
        }
        
        navMeshPathStatus = navMeshAgent.pathStatus;
        if(debug){
            switch(navMeshPathStatus){
                case NavMeshPathStatus.PathComplete:
                    renderer.material.color = Color.red;
                break;

                case NavMeshPathStatus.PathInvalid:
                    renderer.material.color = Color.blue;

                break;

                case NavMeshPathStatus.PathPartial:
                    renderer.material.color = Color.green;
                break;
            }
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.transform.GetComponent<Bullet>()){
            other.transform.GetComponent<Bullet>().KillBullet();
            Hit(1);   
        }
    }

    public void Hit(int dammage){
        lifepoint -= dammage;
        if(scaleBonce != null && scaleBonce.IsPlaying()){
            scaleBonce.Kill(false);
        }
        hit_feedback.PlayFeedbacks();
        transform.localScale = originalScale;
        scaleBonce = transform.DOScale(0.4f, 0.15f).SetRelative(true).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutQuad);

        if(lifepoint <= 0){
            Die();
        }
    }

    public void Die(){
        if(scaleBonce != null && scaleBonce.IsPlaying()){
            scaleBonce.Kill(false);
        }
        dead = true;
        navMeshAgent.isStopped = true;
        CanMove = false;
        animator.SetTrigger("Die");
        GameControlleur.playerStartRun -= StartMove;

        transform.DOScale(0.3f, 0.2f).SetRelative(true).SetEase(Ease.OutQuad).OnComplete(() =>{
            dead_feedback.PlayFeedbacks();
            GameControlleur.Instance.RemoveEnnemyToList(this);
            Destroy(gameObject);
        });
    }

    private void OnDestroy() {
        GameControlleur.playerStartRun -= StartMove;
    }

    private void OnDisable() {
        GameControlleur.playerStartRun -= StartMove;
    }

    private void OnDrawGizmosSelected() {
        if(activateDebug){
            Gizmos.color = colorGizmos;
            Gizmos.DrawWireSphere(transform.position, maxDistanceBeforeNavMeshMovement / 2f);
        }
    }
}
