﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPreviewDisplayer : MonoBehaviour
{
    public static ShopPreviewDisplayer Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void DisplayEquipableItem(GameObject EquipableGO,bool isEquipableUnlocked)
    {

        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }

        var neo = Instantiate(EquipableGO, transform);
        neo.SetActive(true);
        neo.transform.localPosition = Vector3.zero;
        //neo.transform.localScale = Vector3.one;

      //  if (ShopManager.Instance.DisableMaterialForLockedItems == true)
      //  {
      //      if (!isEquipableUnlocked)
      //      {
      //          foreach (var mr in GetComponentsInChildren<MeshRenderer>())
      //          {
      //              for (int i = 0; i < mr.materials.Length; i++)
      //              {
      //                  mr.material = null;
      //              }
      //          }
      //      }
      //  }
      //
     
    }
}
