﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MysteryItemPannel : MonoBehaviour
{
    [SerializeField] private GameObject _continueButton;
    [SerializeField] private GameObject _unlockMysteryUnlockableButton;
    [SerializeField] private GameObject _noThanksButton;
    [SerializeField] private Text _mysteryItemPercentage;
    [SerializeField] private GameObject _mysteryImage;
    [SerializeField] private GameObject _previewImage;
    [SerializeField] private Image _mysteryFillingImage;
    [SerializeField] private ParticleSystem _confettiParticles;


    public GameObject ContinueButton => _continueButton;
    public GameObject UnlockMysteryUnlockableButton => _unlockMysteryUnlockableButton;
    public GameObject NoThanksButton => _noThanksButton;
    public Text MysteryItemPercentage => _mysteryItemPercentage;
    public GameObject MysteryImage => _mysteryImage;
    public GameObject PreviewImage => _previewImage;
    public Image MysteryFillingImage => _mysteryFillingImage;

    public void ResetButtonScale()
    {
        _continueButton.transform.localScale = Vector3.zero;
        UnlockMysteryUnlockableButton.transform.localScale = Vector3.zero;
        _noThanksButton.transform.localScale = Vector3.zero;
    }

    public void SetPercentageText(string text)
    {
        _mysteryItemPercentage.text = text;
    }

    public void SetGiftFillAmount(float amount)
    {
        _mysteryFillingImage.fillAmount = amount;

    }

    public void DisplayUnlockButtons()
    {
        LeanTween.cancel(UnlockMysteryUnlockableButton);
        LeanTween.scale(UnlockMysteryUnlockableButton, Vector3.one, 0.3f).setEaseOutBack();

        LeanTween.cancel(_noThanksButton);
        LeanTween.scale(_noThanksButton, Vector3.one, 0.3f).setEaseOutBack();

        _mysteryItemPercentage.gameObject.SetActive(false);
    }

    public void DisplayContinueButton()
    {
        LeanTween.cancel(_continueButton);
        LeanTween.scale(_continueButton, Vector3.one, 0.3f).setEaseOutBack();

        LeanTween.cancel(_noThanksButton);
        LeanTween.scale(_noThanksButton, Vector3.zero, 0.2f).setEaseOutBack();
    }

    public void PlayMysteryGiftUnlockSequence()
    {
        UnlockSequencePartOne();
    }

    private void UnlockSequencePartOne()
    {
        LeanTween.moveLocalX(_mysteryImage, _mysteryImage.transform.localPosition.x - 10f, 0.02f).setLoopPingPong(5);
        LeanTween.moveLocalY(_mysteryImage, _mysteryImage.transform.localPosition.y + 10f, 0.02f).setLoopPingPong(5).setOnComplete(UnlockSequencePartTwo);
        MysteryManager.Instance.LaunchLoopingVibrations(0.2f, 0.04f);
    }

    private void UnlockSequencePartTwo()
    {
        LeanTween.scaleX(_mysteryImage, 0.5f, 0.5f).setEaseOutSine();
        LeanTween.scaleY(_mysteryImage, 1.25f, 0.5f).setEaseOutSine().setOnComplete(UnlockSequencePartThree);
    }

    private void UnlockSequencePartThree()
    {
        LeanTween.scale(_mysteryImage, Vector3.zero, 0.1f);
        _previewImage.SetActive(true);

         _confettiParticles.Play();

        MysteryManager.Instance.OnGiftOpenedSequenceEnded();


        VibrationManager.VibrateSuccess();

        DisplayContinueButton();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
