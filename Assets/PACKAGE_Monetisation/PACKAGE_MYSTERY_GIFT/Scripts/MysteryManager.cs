﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

public class MysteryManager : MonoBehaviourSingleton<MysteryManager>
{
    [SerializeField] private Transform _mysteryGiftHolder;
    private const string LAST_GIFT_INDEX_COUNT = "LastGiftIndex";
 

    #region Fields

  

    public MysteryItemPannel MysteryItemPannel;
    [Space(10), InlineEditor(InlineEditorObjectFieldModes.Boxed)]
    public int totalStepsToGetGift = 5;
    public float mysteryAnimationTime;



    private int totalMysteryGiftCount => ShopManager.Instance.NotUnlockedMysteryItem.Count;





   

    private int currentLevel;
    private int currentMysteryGiftIndex;
    private float giftUnlockedPercentage;


    #endregion


 

    void Update()
    {

#if UNITY_EDITOR
       
        if (Input.GetKeyDown(KeyCode.B))
        {
            DisplayMysteryPanel(100);
        }
#endif
    }

    public void DisplayMysteryPanel()
    {
        UIController.Instance.ShowPanel(UIPanelName.MYSTERY);

        MysteryItemPannel.ResetButtonScale();

        currentLevel = LevelManager.Instance.CurrentLevelIndex + 1;

        currentMysteryGiftIndex = PlayerPrefs.GetInt(LAST_GIFT_INDEX_COUNT, -1) + 1;

        if(currentMysteryGiftIndex >= totalMysteryGiftCount)
        {
            Debug.LogError("No more Mystery Gift to unlock -> current mystery gift index = " + currentMysteryGiftIndex);
            GameAnalytics.NewDesignEvent("No More Mystery Gift To Unlock.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }

        else
        {
            if(currentLevel % totalStepsToGetGift == 0)
            {
                giftUnlockedPercentage = 100f;
            }
            else
            {
                giftUnlockedPercentage = ((currentLevel % totalStepsToGetGift) / (float)totalStepsToGetGift) * 100f;
            }

            MysteryItemPannel.SetPercentageText(giftUnlockedPercentage.ToString() + "%");
            PlayMysterySuccessAnimation();
        }
    }

    public void DisplayMysteryPanel(float openPercentage)
    {
        UIController.Instance.ShowPanel(UIPanelName.MYSTERY,true);

        MysteryItemPannel.ResetButtonScale();

        currentLevel = LevelManager.Instance.CurrentLevelIndex + 1;

        currentMysteryGiftIndex = PlayerPrefs.GetInt(LAST_GIFT_INDEX_COUNT, -1) + 1;

        Debug.LogError("TOTAL MYSTERY GIFT " + totalMysteryGiftCount);

        if (currentMysteryGiftIndex >= totalMysteryGiftCount)
        {
            Debug.LogError("No more Mystery Gift to unlock -> current mystery gift index = " + currentMysteryGiftIndex);
            GameAnalytics.NewDesignEvent("No More Mystery Gift To Unlock.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }

        else
        {
            giftUnlockedPercentage = openPercentage;
            MysteryItemPannel.SetPercentageText(giftUnlockedPercentage.ToString() + "%");
            PlayMysterySuccessAnimation();
        }
    }

    void PlayMysterySuccessAnimation()
    {
        MysteryItemPannel.SetPercentageText((giftUnlockedPercentage - 100 / totalStepsToGetGift).ToString("0") + "%");
        LeanTween.value(MysteryItemPannel.MysteryItemPercentage.gameObject, UpdatePercentageText, giftUnlockedPercentage - 100 / totalStepsToGetGift, giftUnlockedPercentage, mysteryAnimationTime).setEaseInOutSine().setOnComplete(MysteryAnimationComplete);
        LaunchLoopingVibrations(mysteryAnimationTime, 0.2f);
    }

    void UpdatePercentageText(float value)
    {
        MysteryItemPannel.SetPercentageText(value.ToString("0") + "%");
        MysteryItemPannel.SetGiftFillAmount(value / 100f);
    }

    void MysteryAnimationComplete()
    {
        if (giftUnlockedPercentage >= 100f)
        {
            MysteryItemPannel.DisplayUnlockButtons();
        }

        else
        {
            MysteryItemPannel.DisplayContinueButton();
        }
    }
    
    public void UnlockMysteryGift()
    {
        Debug.LogError("UNLOCKING MYSTERY GIFT");

        if (ShopManager.Instance.NotUnlockedMysteryItem.Count > 0)
        {
            ShopItem item = ShopManager.Instance.NotUnlockedMysteryItem[currentMysteryGiftIndex];
        }

        else
        {
            Debug.LogError("NO MYSTERY GIFT IN MYSTERY GIFT ARRAY");
        }

        MysteryItemPannel.PlayMysteryGiftUnlockSequence();
        PlayerPrefs.SetInt(LAST_GIFT_INDEX_COUNT, currentMysteryGiftIndex);
    }

    #region Unlock Sequences

  

    public void OnGiftOpenedSequenceEnded()
    {
        ShopItem_Spawnable item = ShopManager.Instance.NotUnlockedMysteryItem[currentMysteryGiftIndex];
        item.Unlock();
        item.Select();
        item.TryEquip();
        item.gameObject.SetActive(true);

        for (int i = 0; i < _mysteryGiftHolder.childCount; i++)
        {
            Destroy(_mysteryGiftHolder.GetChild(i));
        }

        Instantiate(item.ObjectToSpawn, _mysteryGiftHolder.position, _mysteryGiftHolder.rotation, _mysteryGiftHolder);
    }

    #endregion

    #region  Vibrations
    public void LaunchLoopingVibrations(float duration, float frequency)
    {
        InvokeRepeating("VibrateLight", 0f, frequency);
        Invoke("StopLoopingVibrations", duration);
    }

    void StopLoopingVibrations()
    {
        CancelInvoke("VibrateLight");
    }

    void VibrateLight()
    {
        VibrationManager.VibrateLight();
    }
    #endregion

}
