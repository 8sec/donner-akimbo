﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TR_Chest : MonoBehaviour
{


 

    #region Fields
    [SerializeField] private Transform _skinDisplayerOffset;
    [SerializeField] private GameObject _spinSprite;

    [Header("Particles")]
    [SerializeField] private ParticleSystem _onChestMoneyOpenedParticle;
    [SerializeField] private ParticleSystem _onChestSkinOpenedParticle;

    [Header("Idle Animation")]
    [SerializeField] private GameObject _chestUpObject;
    [SerializeField] private float _openAnimationTime = 0.8f;
    [SerializeField] private float _openessValue = 25;

    [SerializeField] private LeanTweenType _idleAnimationEase;

    [Header("On Open Animation")]
    [SerializeField] private float _onOpenAnimationTime = 0.2f;
    [SerializeField] private LeanTweenType _onOpenAnimationEase;
    [SerializeField] private float _delayAfterAnimation = 0.1f;

    [Header("Leantween params")]
    [SerializeField] private float _scaleUpTime = 0.2f;
    [SerializeField] private float _chestScaleDownTime = 0.2f;
    [SerializeField] private LeanTweenType _scaleUpEase;

    [SerializeField] private Image _moneyIcone;
    [SerializeField] private GameObject _chestVisual;
    [SerializeField] private ChestType _chestType;
    [SerializeField] private Text _moneyText;
    #endregion


    private bool _hasBeenRecolted;
    public bool HasBeenRecolted => _hasBeenRecolted;

    private ShopItem_Spawnable _skinToUnlock;

    private int _moneyAmount;
    public ChestType ChestType => _chestType;

    private Vector3 _localEulerAnglesAtStart;
    private Vector3 _localEulerAnglesTarget;

    private void Start()
    {
        _localEulerAnglesAtStart = _chestVisual.transform.localEulerAngles;
        _localEulerAnglesTarget = _localEulerAnglesAtStart.SetY(_localEulerAnglesAtStart.y + 360);
        float randomDelayBeforeLT = Random.Range(0.1f, 3f);
        LeanTween.rotateLocal(_chestUpObject, Vector3.forward * _openessValue , _openAnimationTime).setEase(_idleAnimationEase).setDelay(randomDelayBeforeLT).setLoopPingPong(1).setOnComplete(OnComplete);
        _moneyText.gameObject.SetActive(false);
        _moneyIcone.enabled = false;
    }

    private void OnComplete()
    {
        float randomWaitTime = Random.Range(2, 5);
        LeanTween.rotateLocal(_chestUpObject, Vector3.forward * _openessValue, _openAnimationTime).setEase(_idleAnimationEase).setDelay(randomWaitTime).setLoopPingPong(1).setOnComplete(OnComplete);

    }

    public void Action()
    {
        Debug.LogError("CALLED CHEST ACTION A");
        if (TR_KeyManager.Instance.HasKeyLeft)
        {
            Debug.LogError("CALLED CHEST ACTION B");
            VibrationManager.VibrateLight();
            LeanTween.value(_chestVisual, OpenAnimationUpdate, 0, 1, _onOpenAnimationTime).setEase(_onOpenAnimationEase).setOnComplete(c =>
            {
                LeanTween.rotateLocal(_chestUpObject, Vector3.forward * 65, 0.1f);
                LeanTween.delayedCall(_delayAfterAnimation, CollectReward);
            });
        }
    }

    public void OpenAnimationUpdate(float t)
    {
        _chestVisual.transform.localEulerAngles = Vector3.Lerp(_localEulerAnglesAtStart, _localEulerAnglesTarget, t);
    }


    public void CollectReward()
    {
        if (TR_KeyManager.Instance.HasKeyLeft == true)
        {
            LeanTween.scale(_chestVisual, Vector3.zero, _chestScaleDownTime);

            if (_hasBeenRecolted == false)
            {
                _hasBeenRecolted = true;

                if (_chestType == ChestType.Money)
                {
                    _spinSprite.SetActive(false);
                    _moneyIcone.enabled = true;
                    _moneyText.gameObject.SetActive(true);

                    _moneyIcone.transform.localScale = Vector3.zero;
                    _moneyText.transform.localScale = Vector3.zero;

                    LeanTween.scale(_moneyIcone.gameObject, Vector3.one, _scaleUpTime).setEase(_scaleUpEase);
                    LeanTween.scale(_moneyText.gameObject, Vector3.one, _scaleUpTime).setEase(_scaleUpEase);
                    Transform particleTransform =  Instantiate(_onChestMoneyOpenedParticle.gameObject, transform.position - transform.forward * 0.1f, Quaternion.identity).transform;
                    particleTransform.forward = -transform.forward;


                    // REPLACE WITH YOUR OWN MONEY HANDLING SCRIPT
                    CurrencyManager.Instance.AddCurrency(_moneyAmount);
                    Debug.LogError("Collected " + _moneyAmount + "Money");
                }

                else
                {
                    // NOT GENERIC STUFF THERE 
                    _spinSprite.SetActive(true);
                    Transform t = Instantiate(_skinToUnlock.ObjectToSpawn, transform.position - transform.forward * 0.3f, Quaternion.identity, _skinDisplayerOffset).transform;
                    t.localPosition = Vector3.zero;
                 
                    Transform particleTransform = Instantiate(_onChestSkinOpenedParticle.gameObject, transform.position - transform.forward * 0.1f, Quaternion.identity).transform;
                    particleTransform.forward = -transform.forward;
                 
                    t.gameObject.SetActive(true);
                    t.localScale = Vector3.zero;
                    Vector3 targetScale = Vector3.one;
                    LeanTween.scale(t.gameObject, targetScale, _scaleUpTime);
                    t.eulerAngles = Vector3.zero;
                    t.localEulerAngles = Vector3.zero.SetY(-90);
                    RotationScript rotScript = t.gameObject.AddComponent<RotationScript>();
                    rotScript.rotationSpeed = Vector3.up * 50;
                    _skinToUnlock.Unlock();
                   Debug.LogError("Collected Skin");
                }

                VibrationManager.VibrateHeavy();
                TR_KeyManager.Instance.UseKey();
            }
        }
    }

    public void SetMoneyChest(int moneyAmout)
    {
        _chestType = ChestType.Money;
        _moneyAmount = moneyAmout;

        if (_moneyText == null)
        {
            return; 
        }

        _moneyText.text = _moneyAmount.ToString();


    }

    public void SetSkinChest(ShopItem_Spawnable associatedSpawnable)
    {
        _chestType = ChestType.Skin;
        _skinToUnlock = associatedSpawnable;

        if (_moneyText == null)
        {
            return;
        }

        _moneyText.gameObject.SetActive(false);
    }
}
