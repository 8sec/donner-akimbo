﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TR_MoreKeysRewardedButton : RewardedButton
{
    public override void CollectReward()
    {
        base.CollectReward();
        TR_KeyManager.Instance.AddKey(3);
    }

    
}
